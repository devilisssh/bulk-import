# frozen_string_literal: true

Rails.application.routes.draw do
  resources :policies
  resources :companies
  resources :employees
  namespace :employee do
    resource :bulk_import, only: %i[new create]
  end

  root to: "employees#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
