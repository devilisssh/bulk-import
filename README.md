## Bulk Import

### Setup

The setup is a pretty standard Rails App setup using the **SQlite DB, Ruby 2.5.1 & Rails 5.2.3**

#### Steps

* Install **Ruby 2.5.1 & SQlite3**

* Clone the repo using 

  `git clone https://devilisssh@bitbucket.org/devilisssh/bulk-import.git`

* Once cloned, cd into the repo directory & run

  `bundle install`

  This will install all the gem necessary to run the app.

* After the gems are installed, run the following commands to setup the DB:

  ` bundle exec rake db:create db:setup`

  This will create the Database, the tables necessary & seed it with a list of companies

* Once done, run the Rails server:

  `bundle exec rails s`

### Usage

* Navigate to `http://localhost:3000` in your browser.

* You should see the following:

![Homepage](./screenshot.png "Homepage")

* Click on the link named `Employee Bulk Import` in the nav bar & you should see a page like this:

![Bulk Import](./bulk_import.png "Homepage")

* Select a company from the companies dropdown.

* Select a CSV to upload & press Import.

* If everything goes well, you should see a success message on the page.

* You can check if the employees/policies were imported correctly on their specific pages.

* The import can fail due to the following reasons:

  * The CSV supplied has invalid headers.
  * Its not a valid CSV file
  * The data in the CSV is invalid, i.e. an email is not of valid format.
    * This will rollback all the records, if any created in the import session
  * The size of the CSV being used is > 2MB

### Running Tests

You can run tests using 

  `bundle exec rspec` or `bundle exec rake spec`
