# frozen_string_literal: true

class Employee::BulkImportsController < ApplicationController
  def new
  end

  def create
    import = EmployeeImporter.call(import_params)

    if import.success?
      redirect_to new_employee_bulk_import_path, notice: "Import Successful"
    else
      flash.now[:error] = import.error
      render :new
    end
  end

  private

    def import_params
      params.require(:data).permit(:file, :company_id)
    end
end
