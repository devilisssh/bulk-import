# frozen_string_literal: true
require "csv"

class EmployeeImporter
  include Interactor::Organizer

  organize EmployeeImporter::Validate, EmployeeImporter::ProcessCsvData
end
