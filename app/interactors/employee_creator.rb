# frozen_string_literal: true

class EmployeeCreator
  include Interactor

  def call
    create_employee!
  end

  private

    def manager
      Employee.find_by(email: context.employee_attributes["Report To"])
    end

    def policies
      policy_names.collect { |name| context.company.policies.find_or_create_by(name: name) }
    end

    def policy_names
      context.employee_attributes["Assigned Policies"].split("|").reject(&:empty?)
    end

    def create_employee!
      context.employee = context.company.employees.create!(
        email: context.employee_attributes["Email"],
        name: context.employee_attributes["Employee Name"],
        phone: context.employee_attributes["Phone"],
        policies: policies,
        parent: manager
      )
    end
end
