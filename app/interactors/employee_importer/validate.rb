# frozen_string_literal: true

class EmployeeImporter::Validate
  include Interactor

  def call
    file_exists!
    company_exists!
    file_is_csv!
    file_size_is_valid!
    file_readable!
    file_attributes_are_valid!
  end

  private

    def file_exists!
      context.fail!(error: "No file specified") unless context.file
    end

    def file_is_csv!
      context.fail!(error: "Invalid file type") unless context.file.content_type.eql?("text/csv")
    end

    def file_size_is_valid!
      context.fail!(error: "File size should be less than 2 MB") if context.file.size > 2.megabytes
    end

    def company_exists!
      context.company = Company.find_by(id: context.company_id)

      context.fail!(error: "Invalid Company") unless context.company
    end

    def file_readable!
      context.csv_data = CSV.read(context.file.tempfile, headers: true)
    rescue CSV::MalformedCSVError, ArgumentError
      context.fail!(error: "Invalid CSV data")
    end

    def file_attributes_are_valid!
      context.fail!(error: "Invalid CSV attributes") unless attributes_valid?
    end

    def attributes_valid?
      (context.csv_data.headers & valid_headers) == valid_headers
    end

    def valid_headers
      ["Employee Name", "Email", "Phone", "Report To", "Assigned Policies"]
    end
end
