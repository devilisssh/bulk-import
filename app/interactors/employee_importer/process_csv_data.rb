# frozen_string_literal: true

class EmployeeImporter::ProcessCsvData
  include Interactor

  def call
    ActiveRecord::Base.transaction do
      context.csv_data.each do |emp_attrs|
        EmployeeCreator.call(company: context.company, employee_attributes: emp_attrs)
      rescue ActiveRecord::RecordInvalid => e
        context.fail!(error: e.message)
      end
    end
  end
end
