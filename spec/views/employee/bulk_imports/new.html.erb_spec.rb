
# frozen_string_literal: true

require "rails_helper"

RSpec.describe("employee/bulk_imports/new", type: :view) do
  it "renders new employee form" do
    render

    assert_select "form[action=?][method=?]", employee_bulk_import_path, "post" do
      assert_select "select#data_company_id[name=?]", "data[company_id]"

      assert_select "input#data_file[name=?]", "data[file]"

      assert_select "input[type='submit'][value=?]", "Import"
    end
  end
end
