# frozen_string_literal: true

require "rails_helper"

RSpec.describe("policies/index", type: :view) do
  before(:each) do
    assign(:policies, [
      Policy.create!(
        name: "Maternity Leave",
        company: Company.create!(name: "Rohan")
      ),
      Policy.create!(
        name: "Caregiver Leave",
        company: Company.create!(name: "Rivendell")
      ),
    ])
  end

  it "renders a list of policies" do
    render
    assert_select "tr>td", text: "Maternity Leave", count: 1
    assert_select "tr>td", text: "Caregiver Leave", count: 1
    assert_select "tr>td", text: "Rivendell", count: 1
    assert_select "tr>td", text: "Rohan", count: 1
  end
end
