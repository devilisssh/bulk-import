# frozen_string_literal: true

require "rails_helper"

RSpec.describe("policies/new", type: :view) do
  before(:each) do
    assign(:policy, Policy.new(
      name: "Leave without Pay",
      company: Company.create!(name: "Helm's Deep")
    ))
  end

  it "renders new policy form" do
    render

    assert_select "form[action=?][method=?]", policies_path, "post" do
      assert_select "input#policy_name[name=?]", "policy[name]"

      assert_select "select#policy_company_id[name=?]", "policy[company_id]"
    end
  end
end
