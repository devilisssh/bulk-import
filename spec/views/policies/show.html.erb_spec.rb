# frozen_string_literal: true

require "rails_helper"

RSpec.describe("policies/show", type: :view) do
  before(:each) do
    @policy = assign(:policy, Policy.create!(
      name: "Pippin",
      company: Company.create!(name: "Took")
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to(match(/Pippin/))
    expect(rendered).to(match(/Took/))
  end
end
