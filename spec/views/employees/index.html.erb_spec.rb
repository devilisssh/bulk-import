require 'rails_helper'

RSpec.describe "employees/index", :type => :view do
  before(:each) do
    assign(:employees, [
      Employee.create!(
        :name => "Boromir",
        :email => "boromir@fellowship.com",
        :phone => "8012367342",
        :company => Company.create!(name: "Humans")
      ),
      Employee.create!(
        :name => "Faramir",
        :email => "captian@gondor.com",
        :phone => "7122334450",
        :company => Company.create!(name: "Gondor")
      )
    ])
  end

  it "renders a list of employees" do
    render
    assert_select "tr>td", :text => "Boromir".to_s, :count => 1
    assert_select "tr>td", :text => "Faramir".to_s, :count => 1
    assert_select "tr>td", :text => "boromir@fellowship.com".to_s, :count => 1
    assert_select "tr>td", :text => "captian@gondor.com".to_s, :count => 1
    assert_select "tr>td", :text => "8012367342".to_s, :count => 1
    assert_select "tr>td", :text => "7122334450".to_s, :count => 1
    assert_select "tr>td", :text => "Humans", :count => 1
    # assert_select "tr>td", :text => "Gondor", :count => 1
  end
end
