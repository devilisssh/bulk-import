require 'rails_helper'

RSpec.describe "employees/show", :type => :view do
  before(:each) do
    @employee = assign(:employee, Employee.create!(
      :name => "Gimli",
      :email => "gimli@fellowship.com",
      :phone => 5555511111,
      :company => Company.create!(name: "Misty Mountains")
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Gimli/)
    expect(rendered).to match(/gimli@fellowship.com/)
    expect(rendered).to match(/5555511111/)
    expect(rendered).to match(/Misty Mountains/)
  end
end
