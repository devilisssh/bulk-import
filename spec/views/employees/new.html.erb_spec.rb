# frozen_string_literal: true

require "rails_helper"

RSpec.describe("employees/new", type: :view) do
  before(:each) do
    assign(:employee, Employee.new(
      name: "Legolas",
      email: "legolas@fellowship.com",
      phone: "9999900000",
      company: Company.create!(name: "Mirkwood")
    ))
  end

  it "renders new employee form" do
    render

    assert_select "form[action=?][method=?]", employees_path, "post" do
      assert_select "input#employee_name[name=?]", "employee[name]"

      assert_select "input#employee_email[name=?]", "employee[email]"

      assert_select "input#employee_phone[name=?]", "employee[phone]"

      assert_select "select#employee_company_id[name=?]", "employee[company_id]"
    end
  end
end
