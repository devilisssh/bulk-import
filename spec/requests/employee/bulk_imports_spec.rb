# frozen_string_literal: true

require "rails_helper"

RSpec.describe("Employee::BulkImports", type: :request) do
  describe "GET new" do
    it "returns a status 200" do
      get "/employee/bulk_import/new"

      expect(response.status).to(be(200))
    end
  end

  describe "POST create" do
    let(:valid_csv) do
      Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/files/valid_sample.csv"), "text/csv")
    end
    let(:invalid_csv) do
      Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/files/invalid_sample.csv"), "text/csv")
    end
    let(:invalid_file) do
      Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/files/invalid_file.pdf"), "application/pdf")
    end
    let(:huge_csv) do
      Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/files/huge_file.csv"), "text/csv")
    end
    let(:invalid_email_csv) do
      Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/files/invalid_email.csv"), "text/csv")
    end

    let(:company) { Company.create(name: "Lothlorien") }

    describe "Successful" do
      it "should redirect to new action" do
        post "/employee/bulk_import", params: { data: { company_id: company.id, file: valid_csv } }

        expect(response).to be_redirect
        expect(response).to redirect_to(new_employee_bulk_import_path)
        expect(flash[:notice]).to eq("Import Successful")
      end
    end

    describe "Unsuccessful" do
      context "when company_id invalid" do
        it "shows an error" do
          post "/employee/bulk_import", params: { data: { company_id: "invalid_id", file: valid_csv } }

          expect(response.body).to(include("Invalid Company"))
        end
      end

      context "when file is invalid" do
        it "shows an error" do
          post "/employee/bulk_import", params: { data: { company_id: company.id, file: invalid_csv } }

          expect(response.body).to(include("Invalid CSV data"))
        end
      end

      context "when file extension is invalid" do
        it "shows an error" do
          post "/employee/bulk_import", params: { data: { company_id: company.id, file: invalid_file } }

          expect(response.body).to(include("Invalid file type"))
        end
      end

      context "when file size > 2MB" do
        it "shows an error" do
          post "/employee/bulk_import", params: { data: { company_id: company.id, file: huge_csv } }

          expect(response.body).to(include("File size should be less than 2 MB"))
        end
      end

      context "when file is not uploaded" do
        it "shows an error" do
          post "/employee/bulk_import", params: { data: { company_id: company.id, file: nil } }

          expect(response.body).to(include("No file specified"))
        end
      end

      context "when an email is invalid" do
        it "shows an error" do
          post "/employee/bulk_import", params: { data: { company_id: company.id, file: invalid_email_csv } }

          expect(response.body).to(include(CGI.escapeHTML("Validation failed: Email 'aryaexample.com' is invalid")))
        end
      end
    end
  end
end
