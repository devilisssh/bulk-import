
# frozen_string_literal: true

require "rails_helper"

RSpec.describe Employee::BulkImportsController, type: :routing do
  describe "routing" do
    it "routes to #new" do
      expect(get: "/employee/bulk_import/new").to route_to("employee/bulk_imports#new")
    end

    it "routes to #create" do
      expect(post: "/employee/bulk_import").to route_to("employee/bulk_imports#create")
    end
  end
end
