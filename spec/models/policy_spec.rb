require 'rails_helper'

RSpec.describe Policy, :type => :model do
  describe "validations" do
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name).scoped_to(:company_id) }
  end

  describe "Associations" do
    it { should belong_to(:company) }
    it { should have_and_belong_to_many(:employees) }
  end

end
