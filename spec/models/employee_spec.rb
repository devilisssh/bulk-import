require 'rails_helper'

RSpec.describe Employee, :type => :model do

  describe "validations" do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:email) }
    it { should validate_uniqueness_of(:email) }
  end

  describe "Associations" do
    it { should belong_to(:company) }
    it { should have_and_belong_to_many(:policies) }
  end
end
