# frozen_string_literal: true

require "rails_helper"

RSpec.describe EmployeeImporter::Validate do
  describe ".call" do
    let(:company) { Company.create!(name: "Fangorn Forest") }
    let(:valid_csv) do
      Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/files/valid_sample.csv"), "text/csv")
    end
    let(:invalid_csv) do
      Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/files/invalid_sample.csv"), "text/csv")
    end
    let(:invalid_file) do
      Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/files/invalid_file.pdf"), "application/pdf")
    end
    let(:huge_csv) do
      Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/files/huge_file.csv"), "text/csv")
    end
    let(:invalid_attributes_csv) do
      Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/files/invalid_attributes.csv"), "text/csv")
    end

    describe "Company" do
      context "when company_id invalid" do
        it "should return an error" do
          import = EmployeeImporter::Validate.call(company_id: "invalid_id", file: valid_csv)

          expect(import.success?).to be_falsey
          expect(import.error).to eq("Invalid Company")
        end
      end

      context "when company_id valid" do
        it "should return success" do
          import = EmployeeImporter::Validate.call(company_id: company.id, file: valid_csv)

          expect(import.success?).to be_truthy
          expect(import.error).to be_nil
        end
      end
    end

    describe "File" do
      context "when Invalid csv data" do
        it "should return an error" do
          import = EmployeeImporter::Validate.call(company_id: company.id, file: invalid_csv)

          expect(import.success?).to be_falsey
          expect(import.error).to eq("Invalid CSV data")
        end
      end

      context "when invalid csv attributtes" do
        it "should return an error" do
          import = EmployeeImporter::Validate.call(company_id: company.id, file: invalid_attributes_csv)

          expect(import.success?).to be_falsey
          expect(import.error).to eq("Invalid CSV attributes")
        end
      end

      context "when Invalid file type" do
        it "should return an error" do
          import = EmployeeImporter::Validate.call(company_id: company.id, file: invalid_file)

          expect(import.success?).to be_falsey
          expect(import.error).to eq("Invalid file type")
        end
      end

      context "when csv size > 2MB" do
        it "should return an error" do
          import = EmployeeImporter::Validate.call(company_id: company.id, file: huge_csv)

          expect(import.success?).to be_falsey
          expect(import.error).to eq("File size should be less than 2 MB")
        end
      end

      context "when valid csv data" do
        it "should return success" do
          import = EmployeeImporter::Validate.call(company_id: company.id, file: valid_csv)

          expect(import.success?).to be_truthy
          expect(import.error).to be_nil
        end
      end
    end
  end
end
