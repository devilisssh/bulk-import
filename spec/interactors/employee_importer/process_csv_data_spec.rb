# frozen_string_literal: true

require "rails_helper"

RSpec.describe EmployeeImporter::ProcessCsvData do
  describe ".call" do
    let(:company) { Company.create!(name: "Erebor") }
    let(:csv_data) do
      CSV.read(Rails.root.join("spec/fixtures/files/valid_sample.csv"), headers: true)
    end
    let(:invalid_email_data) do
      CSV.read(Rails.root.join("spec/fixtures/files/invalid_email.csv"), headers: true)
    end

    it "imports new employees" do
      expect(Employee.count).to be_zero
      import = EmployeeImporter::ProcessCsvData.call(company: company, csv_data: csv_data)

      expect(import.success?).to be_truthy
      expect(Employee.count).to eq(3)
    end

    it "creates new employees specified in the csv" do
      import = EmployeeImporter::ProcessCsvData.call(company: company, csv_data: csv_data)

      expect(import.success?).to be_truthy
      expect(Employee.all.collect(&:email)).to include("jon@example.com", "arya@example.com", "bob@example.com")
    end

    it "creates new policy if one does not exist" do
      expect(company.policies.find_by(name: "Sick Leave")).to be_nil

      import = EmployeeImporter::ProcessCsvData.call(company: company, csv_data: csv_data)

      expect(import.success?).to be_truthy
      expect(company.policies.find_by(name: "Sick Leave")).to be_present
    end

    it "does not create a new policy if one exists" do
      company.policies.create!(name: "Sick Leave", created_at: 3.days.ago)

      import = EmployeeImporter::ProcessCsvData.call(company: company, csv_data: csv_data)

      expect(import.success?).to be_truthy
      expect(company.policies.find_by(name: "Sick Leave").created_at.to_date).to eq(3.days.ago.to_date)
    end

    context "when invalid data" do
      it "creates no new records" do
        import = EmployeeImporter::ProcessCsvData.call(company: company, csv_data: invalid_email_data)

        expect(import.success?).to be_falsey
        expect(Employee.count).to eq(0)
      end
    end
  end
end
