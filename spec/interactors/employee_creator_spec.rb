# frozen_string_literal: true

require "rails_helper"

RSpec.describe EmployeeCreator do
  describe ".call" do
    let(:company) { Company.create!(name: "Esgaroth") }
    let(:employee_attributes) do
      {
        "Employee Name" => "Bard",
        "Email" => "bard@laketown.com",
        "Report To" => "master@laketown.com",
        "Assigned Policies" => "Sick Leave|King's Leave|Paternity Leave",
        "Phone" => 6666661212,
      }
    end

    it "create a new employee" do
      expect(Employee.count).to eq(0)
      import = EmployeeCreator.call(company: company, employee_attributes: employee_attributes)

      expect(import.success?).to be_truthy
      expect(Employee.count).to eq(1)
    end

    it "should assign policies to the employee" do
      import = EmployeeCreator.call(company: company, employee_attributes: employee_attributes)

      expect(import.success?).to be_truthy
      expect(import.employee.policies.collect(&:name)).to eq(["Sick Leave", "King's Leave", "Paternity Leave"])
    end

    it "should assign a manager to the employee" do
      boss = Employee.create!(name: "Master", email: "master@laketown.com", company: company)
      import = EmployeeCreator.call(company: company, employee_attributes: employee_attributes)

      expect(import.success?).to be_truthy
      expect(import.employee.parent).to eq(boss)
    end

    context "when manager with email doesn't exist" do
      it "should not assign a manager" do
        import = EmployeeCreator.call(company: company, employee_attributes: employee_attributes)

        expect(import.success?).to be_truthy
        expect(import.employee.parent).to be_nil
      end
    end

    context "when policy name is blank" do
      it "should ignore the policy" do
        employee_attributes["Assigned Policies"] = "||Sick Leave"

        import = EmployeeCreator.call(company: company, employee_attributes: employee_attributes)

        expect(import.success?).to be_truthy
        expect(import.employee.policies.collect(&:name)).to eq(["Sick Leave"])
      end
    end

    context "when email is invalid" do
      it "should raise an exception" do
        employee_attributes["Email"] = "INVALID EMAIL"

        expect do
          EmployeeCreator.call(company: company, employee_attributes: employee_attributes)
        end.to raise_error ActiveRecord::RecordInvalid
      end
    end
  end
end
