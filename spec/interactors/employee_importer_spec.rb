# frozen_string_literal: true

require "rails_helper"

RSpec.describe EmployeeImporter do
  describe ".call" do
    let(:company) { Company.create!(name: "Isengard") }
    let(:valid_csv) do
      Rack::Test::UploadedFile.new(Rails.root.join("spec/fixtures/files/valid_sample.csv"), "text/csv")
    end
    let(:context) { Interactor::Context.new(company_id: company.id, file: valid_csv) }

    it "invokes validation & process csv interactors" do
      expect(EmployeeImporter.organized).to eq([EmployeeImporter::Validate, EmployeeImporter::ProcessCsvData])

      expect(EmployeeImporter::Validate).to receive(:call!).once.with(context)
      expect(EmployeeImporter::ProcessCsvData).to receive(:call!).once.with(context)

      EmployeeImporter.call(company_id: company.id, file: valid_csv)
    end
  end
end
